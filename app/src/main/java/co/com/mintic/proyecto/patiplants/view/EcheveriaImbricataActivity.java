package co.com.mintic.proyecto.patiplants.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import co.com.mintic.proyecto.patiplants.R;

public class EcheveriaImbricataActivity extends AppCompatActivity {

    private FloatingActionButton Pasar4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_echeveria_imbricata);

        initUI();
    }
    private void initUI(){
        Pasar4 = findViewById(R.id.floating_action_button4);
        Pasar4.setOnClickListener(v -> onPasar1Click());

    }

    private void onPasar1Click() {
        //numero2.setText("1");
        Intent intent = new Intent(EcheveriaImbricataActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}