package co.com.mintic.proyecto.patiplants.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import co.com.mintic.proyecto.patiplants.R;

public class CrassulaPerforataActivity extends AppCompatActivity {

    private FloatingActionButton Pasar3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crassula_perforata);

        initUI();
    }
    private void initUI(){
        Pasar3 = findViewById(R.id.floating_action_button3);
        Pasar3.setOnClickListener(v -> onPasar1Click());

    }

    private void onPasar1Click() {
        //numero2.setText("1");
        Intent intent = new Intent(CrassulaPerforataActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}