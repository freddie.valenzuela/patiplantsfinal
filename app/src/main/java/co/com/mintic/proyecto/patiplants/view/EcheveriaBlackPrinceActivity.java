package co.com.mintic.proyecto.patiplants.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import co.com.mintic.proyecto.patiplants.R;

public class EcheveriaBlackPrinceActivity extends AppCompatActivity {

    private FloatingActionButton Pasar1;
    //private TextView numero1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_echeveria_black_prince);

        initUI();

    }
    private void initUI(){
        Pasar1 = findViewById(R.id.floating_action_button1);
        Pasar1.setOnClickListener(v -> onPasar1Click());
        //numero1 = findViewById(R.id.textViewpl1);

    }

    private void onPasar1Click() {
        Intent intent = new Intent(EcheveriaBlackPrinceActivity.this, ProfileActivity.class);
        //numero1.setText("1");
        startActivity(intent);
    }
}
























